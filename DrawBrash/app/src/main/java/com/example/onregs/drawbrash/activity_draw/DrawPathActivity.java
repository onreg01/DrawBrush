package com.example.onregs.drawbrash.activity_draw;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;


import com.example.onregs.drawbrash.data.XYCoordinate;
import com.example.onregs.drawbrash.view.DrawPathView;

import java.util.LinkedList;

public class DrawPathActivity extends AppCompatActivity implements View.OnTouchListener
{

    private DrawPathView drawPointView;
    private LinkedList<XYCoordinate> coordinates = new LinkedList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        drawPointView = new DrawPathView(this);
        setContentView(drawPointView);
        drawPointView.setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        boolean isNeedDraw = false;

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                coordinates.add(new XYCoordinate(event.getX(), event.getY()));
                isNeedDraw = true;
                break;
            case MotionEvent.ACTION_MOVE:
                coordinates.add(new XYCoordinate(event.getX(), event.getY()));
                isNeedDraw = true;
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                drawPointView.savePath();
                coordinates.clear();
                isNeedDraw = false;
                break;
        }

        if(isNeedDraw)
        {
            drawPointView.setCoordinates(coordinates);
            drawPointView.invalidate();
        }

        return true;
    }
}
