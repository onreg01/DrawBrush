package com.example.onregs.drawbrash.data;

public class XYCoordinate {

    private float x;
    private float y;

    public XYCoordinate(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public XYCoordinate()
    {

    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }
}
