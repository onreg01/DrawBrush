package com.example.onregs.drawbrash.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.example.onregs.drawbrash.data.LineCoordinate;
import com.example.onregs.drawbrash.data.XYCoordinate;

import java.util.LinkedList;

public class DrawLineView extends View {

    private Paint paint;
    private LinkedList<LineCoordinate> coordinates;

    private XYCoordinate startCoordinates;
    private XYCoordinate endCoordinates;

    public DrawLineView(Context context) {
        super(context);
        init();

    }

    public DrawLineView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DrawLineView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
        paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(2);
    }

    public void addLineCoordinates(XYCoordinate startCoordinates, XYCoordinate endCoordinates)
    {
        this.startCoordinates = startCoordinates;
        this.endCoordinates = endCoordinates;
    }

    public void setCoordinates(LinkedList<LineCoordinate> coordinates)
    {
        this.coordinates = coordinates;
    }


    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawColor(Color.GREEN);
        if (coordinates != null) {
            for (LineCoordinate coordinate : coordinates) {
                canvas.drawLine(coordinate.getStartCoordinates().getX(), coordinate.getStartCoordinates().getY(),
                        coordinate.getEndCoordinates().getX(), coordinate.getEndCoordinates().getY(), paint);
            }
        }
        if (startCoordinates!=null && endCoordinates != null) {
            canvas.drawLine(startCoordinates.getX(), startCoordinates.getY(), endCoordinates.getX(), endCoordinates.getY(), paint);
        }
    }
}
