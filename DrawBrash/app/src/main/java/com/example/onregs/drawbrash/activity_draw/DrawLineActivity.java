package com.example.onregs.drawbrash.activity_draw;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;

import com.example.onregs.drawbrash.data.LineCoordinate;
import com.example.onregs.drawbrash.data.XYCoordinate;
import com.example.onregs.drawbrash.view.DrawLineView;

import java.util.LinkedList;

public class DrawLineActivity extends AppCompatActivity implements View.OnTouchListener {

    private DrawLineView drawView;

    private LinkedList<LineCoordinate> coordinates = new LinkedList<>();
    private XYCoordinate startCoordinates;
    private XYCoordinate endCoordinates;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        drawView = new DrawLineView(this);
        setContentView(drawView);
        drawView.setOnTouchListener(this);
        drawView.invalidate();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startCoordinates = new XYCoordinate();
                startCoordinates.setX(event.getX());
                startCoordinates.setY(event.getY());
                break;
            case MotionEvent.ACTION_MOVE:
                endCoordinates = new XYCoordinate();
                endCoordinates.setX(event.getX());
                endCoordinates.setY(event.getY());
                drawView.addLineCoordinates(startCoordinates, endCoordinates);
                drawView.invalidate();
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                coordinates.add(new LineCoordinate(startCoordinates, endCoordinates));
                drawView.setCoordinates(coordinates);
                drawView.invalidate();
                break;
        }

        return true;
    }
}
