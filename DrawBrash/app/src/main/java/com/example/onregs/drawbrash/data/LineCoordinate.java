package com.example.onregs.drawbrash.data;

public class LineCoordinate {

    private XYCoordinate startCoordinates;
    private XYCoordinate endCoordinates;

    public LineCoordinate() {

    }

    public LineCoordinate(XYCoordinate startCoordinates, XYCoordinate endCoordinates) {
        this.startCoordinates = startCoordinates;
        this.endCoordinates = endCoordinates;
    }

    public XYCoordinate getStartCoordinates() {
        return startCoordinates;
    }

    public void setStartCoordinates(XYCoordinate startCoordinates) {
        this.startCoordinates = startCoordinates;
    }

    public XYCoordinate getEndCoordinates() {
        return endCoordinates;
    }

    public void setEndCoordinates(XYCoordinate endCoordinates) {
        this.endCoordinates = endCoordinates;
    }
}
