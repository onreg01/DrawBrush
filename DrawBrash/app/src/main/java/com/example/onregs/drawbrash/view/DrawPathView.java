package com.example.onregs.drawbrash.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;


import com.example.onregs.drawbrash.data.XYCoordinate;

import java.util.LinkedList;

public class DrawPathView extends View {

    private Paint paint;
    private Path path;

    private LinkedList<Path> paths = new LinkedList<>();

    public DrawPathView(Context context) {
        super(context);
        init();
    }

    public DrawPathView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DrawPathView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init()
    {
        paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(5);
    }

    public void setCoordinates(LinkedList<XYCoordinate> coordinates) {
        path = new Path();
        path.moveTo(coordinates.getFirst().getX(), coordinates.getFirst().getY());

        for(XYCoordinate coordinate: coordinates)
        {
            path.lineTo(coordinate.getX(), coordinate.getY());
        }
    }

    public void savePath()
    {
        if(path!=null)
        {
            paths.add(path);
        }

    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawColor(Color.GREEN);

        for(Path path:paths)
        {
            canvas.drawPath(path, paint);
        }

        if(this.path!=null)
        {
            canvas.drawPath(this.path, paint);
        }
    }
}
